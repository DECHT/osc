
/*
#include <iostream>
int main() {
    std::cout << "Hello, world" << std::endl;
    return 0;
}
*/

int a[4];
int a[] = {1, 2, 3}; 

int x = 3;
double y = (double) x / 2; // or
double y = double(x) / 2;
// int from -2^31 to 2^31-1
// unsigned int from 0 to 2^32-1

typedef int Index;
Index a = 3;

enum { caps_lock = 4, num_lock = 2, scroll_lock = 1};

struct Rectangle {
    int xp, yp;
    int color;
}
Rectangle r;
r.xp = 100;
r.xp = 100;
r.color = 0;

// Declaration: class keyboard_Controller {}... in file `keyctrl.h`
// Implementation: #include "machine/keyctrl.h"

class Keyboard_Controller {
private:
    unsigned char code;
public:
    Keyboard_Controller(); //Constructor
    ~Keyboard_Controller(); //Destructor
};

