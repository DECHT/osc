#ifndef __screen_include__
#define __screen_include__

/*****************************************************************************/
/* Operating-System Construction                                             */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                             C G A _ S C R E E N                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* This class allows accessing the PC's screen.  Accesses work directly on   */
/* the hardware level, i.e. via the video memory and the graphic adapter's   */
/* I/O ports.                                                                */
/*****************************************************************************/

#include "machine/io_port.h"

class CGA_Screen {
private:
	char *CGA_START = (char *)0xb8000;
	int SCREEN_WIDTH = 80;
	int SCREEN_HIGHT = 25;
	char* position;
	int pos_x, pos_y;
	IO_Port INDEX_PORT(0x3d4); // INDEX_PORT: Write only
	IO_Port DATA_PORT(0x3d5); // DATA_PORT: Read and write

public:
	CGA_Screen(const CGA_Screen &copy) = delete; // prevent copying
	CGA_Screen(); // Constructor
	~CGA_Screen(); // Destructor
	show(int x, int y, char c, unsigned char attrib); // TODO: void necessary?
	setpos(int x, int y);
	getpos(int &x, int &y);
	print(char* text, int length, unsigned char attrib);
};

/* Add your code here */ 

#endif
