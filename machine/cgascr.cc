/*****************************************************************************/
/* Operating-System Construction                                             */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                             C G A _ S C R E E N                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* This class allows accessing the PC's screen.  Accesses work directly on   */
/* the hardware level, i.e. via the video memory and the graphic adapter's   */
/* I/O ports.                                                                */
/*****************************************************************************/

#include "machine/cgascr.h"

/* This method displays the character c at the position (x,y) with the specified
color attribute attrib. Here x specifies the column and y the row of the desired
position, where: 0<=x<=79 and 0<=y<=24. The position (0,0) designates the upper
left corner of the screen. attrib can be used to specify features such as
background color, foreground color and blinking. */ 
void CGA_Screen::show(int x, int y, char c, unsigned char attrib){
    if ( x < 0 || x >= SCREEN_WIDTH) return; // check the width
    if ( y < 0 || y >= SCREEN_HIGHT) return; // check the hight
    
    pos_x = x;
    pos_y = y;

    position = CGA_START + 2*(x + y*SCREEN_WIDTH);
    *position = c;
    *(position + 1) = attrib;
}

/* This method sets the cursor to the specified position.*/
void CGA_Screen::setpos(int x, int y){
    if ( x < 0 || x >= SCREEN_WIDTH) return; // check the width
    if ( y < 0 || y >= SCREEN_HIGHT) return; // check the hight
    // TODO

}

/* Get the current position of the cursor from the graphics card */
void CGA_Screen::getpos(int &x, int &y){
    int current_pos = DATA_PORT.inb();
    x = current_pos % SCREEN_WIDTH;
    y = current_pos / SCREEN_WIDTH; // integer expected;
}

/* This method can be used to output a string text, starting at the current
position of the cursor. Since the string does not need to contain a zero terminator,
as is usually the case with C, the length parameter is required, which must specify
of how many characters text consists. After the output is finished, the cursor
should be placed behind the last printed character. The whole text should be
displayed uniformly with the colors chosen by attrib.
If there is not enough space left until the end of the line, the output should be
continued on the following line. As soon as the last screen line is filled, the
entire screen area should be moved up by one line. This makes the first line
disappear. Now, the last line can be deleted and the output can be continued there.
A line break must also be made whenever the character '\n' is contained in the text. */
void print(char* text, int length, unsigned char attrib){}
